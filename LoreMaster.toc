## Interface: 70300
## Title: |cff00ccffLore Master|r
## Author: Mathew Manton
## Notes: an addon which gives you the lore to all your favourite NPCs, enemies, dungeons and zones.
## DefaultState: enabled
## Version: 1.0
## SavedVariables: LoreMasterDB

config.lua
timer.lua
init.lua